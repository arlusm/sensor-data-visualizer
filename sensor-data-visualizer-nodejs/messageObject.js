module.exports =
    class MessageObject {
    _longitude;
    _latitude;
    _stringAngle;
    _id;
    _direction;
    _endLong;
    _endLat;
    _messageAge;
    constructor() {
    }


    get messageAge() {
        return this._messageAge;
    }

    set messageAge(value) {
        this._messageAge = value;
    }

    set endLong(value) {
        this._endLong = value;
    }

    set endLat(value) {
        this._endLat = value;
    }

    set longitude(value) {
        this._longitude = value;
    }

    set latitude(value) {
        this._latitude = value;
    }

    set stringAngle(value) {
        this._stringAngle = value;
    }

    set id(value) {
        this._id = value;
    }

    set direction(value) {
        this._direction = value;
    }

    get longitude() {
        return this._longitude;
    }

    get latitude() {
        return this._latitude;
    }

    get stringAngle() {
        return this._stringAngle;
    }

    get id() {
        return this._id;
    }

    get direction() {
        return this._direction;
    }
}
