const Observable = require('rxjs/Observable').Observable;
require('rxjs/add/observable/from');
require('rxjs/add/operator/concatMap');
require('rxjs/add/operator/startWith');
require('rxjs/add/operator/pairwise');
require('rxjs/add/operator/delayWhen');
require('rxjs/add/operator/timestamp');
require('rxjs/add/operator/delay');
require('rxjs/add/observable/of');

var WebSocketServer = require('websocket').server;
var http = require('http');
var proj4 = require('proj4');
var MessageObject = require('./messageObject');
var messageObjects = [];
var fs = require('fs');
const LINE_LENGTH = 60;
const csv = require('csv-parser');
const MongoClient = require('mongodb').MongoClient;
const uri = "mongodb+srv://visualizer:visualizePW@prolab-z62as.mongodb.net/test?retryWrites=true&w=majority";
let collection;
let replaying = false;
let disposable;
function parseSensorLocationsFromFile() {

    fs.createReadStream('sensors.txt')
        .pipe(csv())
        .on('data', function(data){
            try {
                addSensorFromFile(data.ID, data.LAT, data.LONG, data.DIRECTION);
            }
            catch(err) {
                console.error(err);
            }
        })
        .on('end',function(){
        });
}

MongoClient.connect(uri,{ useNewUrlParser: true, useUnifiedTopology: true }, function(err, client) {
    if(err) {
        console.log('Error occurred while connecting to MongoDB Atlas...\n',err);
    }
    console.log('Connected to database...');
    parseSensorLocationsFromFile();
    collection = client.db("prolab").collection("sensordata2");

});
var server = http.createServer(function(request, response) {
});
server.listen(3000, function() { });

const wsServer = new WebSocketServer({
    httpServer: server
});

let messageObject = new MessageObject();

wsServer.on('request', function(request) {
    var connection = request.accept(null, request.origin);

    connection.on('message', function(message) {
        if (message !== undefined) {
            if (JSON.parse(message.utf8Data)._type !== undefined) {
                if (JSON.parse(message.utf8Data)._type === 'stop') {
                    replaying = false;
                    disposable.unsubscribe();
                } else {
                    replayData = JSON.parse(message.utf8Data);
                    let start = new Date(replayData.startTime);
                    let end = new Date(replayData.endTime);
                    replaying = true;
                    requestFromDatabase(start, end);

                }
            } else {
                if (!replaying) {
                    wsServer.broadcast(JSON.stringify(createMessageObject((message.utf8Data))));
                }
            }
        }
    });
});
    function addDataToDatabase(longitude, latitude, endLong, endLat, id, messageAge) {
        const date = new Date();
        console.log(date);
        collection.insertOne({ _longitude:longitude, _latitude:latitude, _endLong:endLong,
            _endLat:endLat, id:id, date:date, _messageAge:messageAge });
    }

    function requestFromDatabase(startDateAndTime, endDateAndTime) {
       let fetchedData = collection.find({"date": {
                $gte: startDateAndTime,
                $lt: endDateAndTime
       }
       }).toArray(function (err, doc) {
           if (err) {
               console.error(err);
           } else {
               fetchedData = doc;
               replay(fetchedData);
           }
       });
    }

    function replay(fetchedData) {
        fetchedData = Observable.from(fetchedData);
        disposable = fetchedData
            .startWith(null)
            .pairwise()
            .concatMap(([previous, current]) => previous ?
                Observable.of(current).delay(current.date - previous.date) :
                Observable.of(current)
            )
            .subscribe(replayAngle => wsServer.broadcast(JSON.stringify(replayAngle)));

    }

    function addSensorFromFile(id, latitude, longitude, direction) {
        let sensor = new MessageObject();
        sensor.id = id;
        sensor.direction = direction;
        sensor.longitude = longitude;
        sensor.latitude = latitude;
        messageObjects.push(sensor);
    }

    function createMessageObject(message) {
       const jsonObject = JSON.parse(message);

       // angle 307 means that location wasn't determined
       if (jsonObject.stringAngle === '307') {
           messageObject.id = jsonObject.id;
           return null;
       }


       messageObject.id = jsonObject.id;
       messageObject.stringAngle = jsonObject.stringAngle;
       messageObject.messageAge = jsonObject.messageAge;
       //trimming the zeroes
       messageObject.id = messageObject.id.replace('000000000000', '');

        // find the matching MessageObject for the received data
        const matchingObject = messageObjects.find(obj => {
            return obj.id === messageObject.id;
        });

        messageObject.direction = matchingObject.direction;
        messageObject.longitude = matchingObject.longitude;
        messageObject.latitude = matchingObject.latitude;

        return calculateLineEndCoordinates(messageObject);
    }

    function calculateLineEndCoordinates(messageObject) {
        const beginningLong = parseInt(messageObject.longitude, 10);
        const beginningLat = parseInt(messageObject.latitude, 10);
        const direction = messageObject.direction;
        const angle = messageObject.stringAngle;

        if (beginningLong === undefined || beginningLat === undefined) {
            console.warn('Sensor longitude or sensor latitude was undefined.');
            return "";
        }

        //stringAngles are in between [90;270] where max.left is 270
        //-90 + angle gives us a range [0;180] which is suitable for calculations
        const newAngle = (-90 + angle + direction);
        const endLongCoordinate = beginningLong + ((Math.cos((Math.PI / 180) * newAngle ) * LINE_LENGTH));
        const endLatCoordinate = beginningLat + ((Math.sin((Math.PI / 180) * newAngle ) * LINE_LENGTH));

        const utm = '+proj=utm +zone=35';
        const wgs84 = '+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs';
        // Converting UTM to WGS84
        const beginningXY = (proj4(utm, wgs84, [beginningLong, beginningLat]));
        const endXY = (proj4(utm, wgs84, [endLongCoordinate, endLatCoordinate]));
        messageObject.endLong = endXY[0];
        messageObject.endLat = endXY[1];
        messageObject.longitude = beginningXY[0];
        messageObject.latitude = beginningXY[1];
        addDataToDatabase(messageObject.longitude, messageObject.latitude, endXY[0], endXY[1]
            ,messageObject.id, messageObject.messageAge);

        return messageObject;

    }



