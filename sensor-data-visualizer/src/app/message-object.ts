export class MessageObject {
  public _longitude;
  public _latitude;
  public _endLong;
  public _endLat;
  public _id;
  public _stringAngle;
  public source;
  public direction;
  public messageAge;
}
