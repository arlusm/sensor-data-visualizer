import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {DisplayComponent} from './display/display.component';
import {LatencyChartComponent} from './latency-chart/latency-chart.component';
const routes: Routes  = [
  {path: '', component: DisplayComponent},
  {path: 'latency', component: LatencyChartComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
