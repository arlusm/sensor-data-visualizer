import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.less']
})
export class ModalComponent implements OnInit {

  form: FormGroup;
  startDate: string;
  endDate: string;

  constructor(
    private fb: FormBuilder,
    private dialogRef: MatDialogRef<ModalComponent>,
    @Inject(MAT_DIALOG_DATA) data) {
  }

  ngOnInit() {
    this.form = this.fb.group({
      startDate: [this.startDate, []],
     endDate: [this.endDate, []],
    });
  }

  replay() {
    this.dialogRef.close(this.form.value);
  }

  stopReplay() {
    this.dialogRef.close('stop');
  }
}
