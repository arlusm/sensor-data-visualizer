import {Component, OnInit} from '@angular/core';
import Chart from 'chart.js';
import {webSocket} from 'rxjs/webSocket';

@Component({
  selector: 'app-latency-chart',
  templateUrl: './latency-chart.component.html',
  styleUrls: ['./latency-chart.component.less']
})
export class LatencyChartComponent implements OnInit {
  public latencyChart;
  public numberOfDataPoints = 50;
  public socket = webSocket('ws://localhost:3000');
  public initializedSourcesMap = new Map<string, number>();
  public borderColors = ['#FF3333', '#FFE933', '#5EFF33', '#33FFE3', '#335BFF', '#F033FF', '#FFB233', '#FF3364'];
  private _pausedLoading = false;

  changePausedLoading() {
    this._pausedLoading = !this._pausedLoading;
  }
  constructor() { }

  ngOnInit() {
    this.initialize();
    this.receiveMessages();
  }


  receiveMessages() {
    this.socket.subscribe(
      msg => this.sortData(msg),
    );
  }

  initialize() {
    const ctx = 'latencyChart';
    this.latencyChart = new Chart(ctx, {
      type: 'line',
      data: {
        labels: [],
        datasets: []
      },
      options: {
        scales: {
          xAxes: [{
            display: true,
            bounds: 'data',
           type: 'time',
            time: {
              displayFormats: {
                second: 'HH:mm:ss',
                minute: 'HH:mm',
                hour: 'HH'
              }
            }
          }],
          yAxes: [{
            display: true
          }],
        }
      }
    });
  }

  /**
   *  Assigns line colors to data from different sensors
   *  @param data - received object containing sensor data
   */
  sortData(data) {
    if (data == null) {
      return;
    }
    // for replay
    if (data._id.toString().length > 16) {
      data._id = data.id;
    }
    // If the sensor source hasn't been added to initializedSourcesMap,
    // then it will be assigned to a dataset with a random color and will be added to the initializedSourcesMap
    if (this.initializedSourcesMap.get(data._id) !== 1) {
      const random = this.borderColors[Math.floor(Math.random() * this.borderColors.length)];
      this.latencyChart.chart.data.datasets.push({
        label: data._id,
        borderColor: random,
        data: []
      });
      // the used color gets removed from the borderColors array
      this.borderColors.splice(this.borderColors.indexOf(random), 1);
      this.initializedSourcesMap.set(data._id, 1);
      this.addData(data);
    } else {
      this.addData(data);
    }
  }
  /**
   *  Adds received data to the latency chart
   *  @param data - received object containing sensor data
   */
  addData(data) {
    if (!this._pausedLoading) {
      const label = new Date();
      if (this.latencyChart.data.labels.length >= this.numberOfDataPoints) {
        this.latencyChart.data.labels.shift();
        this.latencyChart.data.labels.shift();

        this.latencyChart.data.datasets.forEach((dataset) => {
          if (data._id === dataset.label) {
            dataset.data.shift();
          }
        });
        this.latencyChart.update();
      }
      if (this.latencyChart.data.datasets.data === undefined) {
        this.latencyChart.data.datasets.data = [data._messageAge];
      }
      this.latencyChart.data.datasets.forEach((dataset) => {
        if (dataset.data !== undefined && data._id === dataset.label) {
          // Delay(ms) for Y-axis and current time to X-axis
          dataset.data.push(data._messageAge);
          this.latencyChart.data.labels.push(label);
        }
      });
      this.latencyChart.update();
    }
  }

}
