import { Component, OnInit } from '@angular/core';
import * as mapboxgl from 'mapbox-gl';
import {environment} from '../../environments/environment';
import * as turf from '@turf/helpers';
import { webSocket } from 'rxjs/webSocket';
import {MatDialog, MatDialogConfig} from '@angular/material';
import {ModalComponent} from '../modal/modal.component';
import {ReplayObject} from '../replay-object';


@Component({
  selector: 'app-display',
  templateUrl: './display.component.html',
  styleUrls: ['./display.component.less']
})

export class DisplayComponent implements OnInit {
  public socket = webSocket('ws://localhost:3000');
  map: mapboxgl.Map;
  style = 'mapbox://styles/mapbox/streets-v11';
  defaultLat = 59.3945;
  defaultLng = 24.672;
  darkMode = false;
  public sensorAngles = {
    'type': 'FeatureCollection',
    'features': []
  };
  public sensorLocations = {
    'type': 'FeatureCollection',
    'features': []
  };

  constructor(private dialog: MatDialog) {}

  ngOnInit() {
    this.buildMap();
    this.receiveMessages();
    this.initDarkMode();
  }

  initDarkMode() {
    if (document.body.getAttribute('data-theme') === 'dark') {
      this.darkMode = true;
    }
  }

  receiveMessages() {
    this.addLineInitialize(this.map);
    this.socket.subscribe(
      msg => this.addTo(msg),
    );
  }

  addTo(msg) {
    if (msg != null) {
      // if the data is being replayed
      if (msg._id.toString().length > 16) {
        this.addLineStrings(this.map, msg._longitude, msg._latitude, msg._endLong, msg._endLat, msg.id);
      } else {
        this.addLineStrings(this.map, msg._longitude, msg._latitude, msg._endLong, msg._endLat, msg._id);
      }
      }
  }

  buildMap() {
    if (this.darkMode) {
      this.style = 'mapbox://styles/mapbox/dark-v10';
    } else {
      this.style = 'mapbox://styles/mapbox/streets-v11';
    }
    (mapboxgl as typeof mapboxgl).accessToken = environment.mapbox.accessToken;
    this.map = new mapboxgl.Map({
      container: 'map',
      style: this.style,
      zoom: 15,
      center: [this.defaultLng, this.defaultLat]
    });
  }

  /**
   *  Changes the layout of the page to either night mode or day mode
   */
  switchLayer() {
    if (this.darkMode) {
      document.body.removeAttribute('data-theme');
      this.map.setStyle('mapbox://styles/mapbox/streets-v11');
      this.darkMode = false;
    } else {
      document.body.setAttribute('data-theme', 'dark');
      this.map.setStyle('mapbox://styles/mapbox/dark-v10');
      this.darkMode = true;
    }
  }

  /**
   *  Tells the server to replay certain timeperiod
   * @param startDateTime - time of replay start
   * @param endDateTime - time of replay end
   */
  replay(map, startDateTime, endDateTime) {
    const replayObject = new ReplayObject();
    if (startDateTime === 'stop') {
      replayObject._type = 'stop';
    } else {
      replayObject._type = 'start';
      replayObject.startTime = startDateTime;
      replayObject.endTime = endDateTime;
    }
    this.sensorAngles.features.splice(0, this.sensorAngles.features.length);
    this.sensorLocations.features.splice(0, this.sensorLocations.features.length);
    map.getSource('measure-lines').setData(this.sensorAngles);
    map.getSource('sensor-locations').setData(this.sensorLocations);
    this.socket.next(replayObject);
  }
  /**
   *  Displays the sensor given angle as a line on the map
   * @param map - instance of the mapbox map
   * @param startLongitude - start longitude of the line
   * @param startLatitude - start latitude of the line
   * @param endLongitude - end longitude of the line
   * @param endLatitude - end latitude of the line
   * @param source - the id of the sensor which sent the information
   */
  public addLineStrings(map, startLongitude, startLatitude, endLongitude, endLatitude, source) {

    if (map.getSource('measure-lines') !== undefined) {
     this.addSensorLocationSymbol(map, startLongitude, startLatitude, source);
      const linestring1 = turf.lineString([[startLongitude, startLatitude], [endLongitude, endLatitude]],
        {name: '' + source, date: new Date()}, {id: '' + source}, );
      this.removeSameSourceLines(source);
      this.removeUnUpdatedLines(5);
      this.sensorAngles.features.push(linestring1);
      map.getSource('measure-lines').setData(this.sensorAngles);
    }
  }

  /**
   *  Removes the previous drawn line of the given sensor
   *  @param source - the id of a sensor
   */
  public removeSameSourceLines(source) {
    if (this.sensorAngles.features.find(obj => obj.id === '' + source)) {
      const index = this.sensorAngles.features.findIndex(obj => obj.id === '' + source);
      this.sensorAngles.features.splice(index, 1);
    }
  }

  public removeUnUpdatedLines(time) {
    if (this.sensorAngles.features.find(obj => (new Date().valueOf() - obj.properties.date.valueOf()) / 1000 > time)) {
    const index = this.sensorAngles.features.findIndex(obj => (new Date().valueOf() - obj.properties.date.valueOf()) / 1000 > time);
    this.sensorAngles.features.splice(index, 1);
    }
  }

  /**
   *  Checks whether a sensor has been added to map, if it hasn't then it's added.
   *  @param map - instance of the mapbox map
   *  @param startLongitude - sensor longitude
   *  @param startLatitude - sensor latitude
   *  @param source - the id of a sensor
   */
  public addSensorLocationSymbol(map, startLongitude, startLatitude, source) {
    if (!this.sensorLocations.features.find(obj => obj.properties.name === source)) {
      const sensorLocation = turf.point([startLongitude, startLatitude], {name: source});
      this.sensorLocations.features.push(sensorLocation);
      map.getSource('sensor-locations').setData(this.sensorLocations);
    }
  }

  openDialog() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;

    const modal = this.dialog.open(ModalComponent, dialogConfig);

    modal.afterClosed().subscribe(
      data => this.parseInput(data)
    );
  }
  /**
   *  Parse entered input to necessary form
   *  @param data - start and end dates and times of replay
   */
  parseInput(data) {
    if (data === undefined) {
      return;
    }
    if (data.toString() === 'stop') {
      this.replay(this.map, 'stop', 'stop');
      return;
    }
    let start = data.startDate.trim();
    let end = data.endDate.trim();
    start = start.replace(' ', 'T');
    end = end.replace(' ', 'T');
    start += '.000Z';
    end += '.000Z';
    this.replay(this.map, start, end);


  }
  /**
   *  Initializes the map
   *  @param map - instance of the mapbox map
   */
  public addLineInitialize(map) {
    map.on('load', function () {
        map.addLayer({
          id: 'measure-lines',
          type: 'line',
          'source': {
            'type': 'geojson',
            'data': false,
          },
          layout: {
            'line-cap': 'round',
            'line-join': 'round',
          },
          paint: {
            'line-color': '#910',
            'line-width': 2,
          },
          filter: ['in', '$type', 'LineString']
        });
      map.addLayer({
        'id': 'sensor-locations',
        'type': 'circle',
        'source': {
          'type': 'geojson',
          'data': false,
        },
        'paint': {
          'circle-color': '#0b0f26',
          'circle-blur': 0.40,
          'circle-radius': {
            'base': 1.99,
            // increase circle size when zooming
            'stops': [[12, 2], [22, 180]]
          },
        }
      });

      map.on('click', 'sensor-locations', function(e) {
        new mapboxgl.Popup()
          .setLngLat(e.lngLat)
          .setHTML(e.features[0].properties.name)
          .addTo(map);
      });

      map.on('mouseenter', 'sensor-locations', function() {
        map.getCanvas().style.cursor = 'pointer';
      });

      map.on('mouseleave', 'sensor-locations', function() {
        map.getCanvas().style.cursor = '';
      });
    });
  }
}
